﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Converts the string into a binary array without messing with Encoding.
        /// </summary>
        /// <param name="str">"This" string to cast.</param>
        /// <returns>Binary array of unmodified data.</returns>
        public static byte[] GetBytes(this string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        /// <summary>
        /// Converts binary data to a string using default encoding.
        /// </summary>
        /// <param name="bytes">"this" binary array to cast.</param>
        /// <returns>The string made out of the unmodified binary data.</returns>
        public static string GetString(this byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
    }
}
