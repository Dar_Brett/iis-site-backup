﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace iis_site_backup
{
    class Program
    {
        private const string Seperator = "--------------------------------------------------------------------------------";
        private static void Main(string[] args)
        {
            Trace.AutoFlush = true;
            Trace.Listeners.Clear();
            Trace.Listeners.Add(new ConsoleTraceListener());
            Trace.Listeners.Add(new TextWriterTraceListener("log.txt"));
            if (args.Count() < 2)
            {
                Trace.WriteLine("Not enough arguments to execute.");
                Trace.WriteLine("First argument must be the export path, followed by sites to be archived.");
                Trace.WriteLine(Seperator);
                return;
            }

            Trace.WriteLine(string.Format("Beginning archive process at {0}", DateTime.Now.ToString(CultureInfo.GetCultureInfo("en-AU").DateTimeFormat)));
            string ArchivePath = args[0];
            Trace.WriteLine("Archiving to {0}", ArchivePath);

            if (!Directory.Exists(ArchivePath))
            {
                Trace.WriteLine("'{0}' does not exist. Ensure this is the directory you intended to archive to, and that it exists and that this server has access to it.");
                Trace.WriteLine(Seperator);
            }

            // All of the sites to try export.
            string[] toArchive = args.Skip(1).ToArray();


            using (var pipe = new NamedPipeServerStream(Shared.Constants.PipeName, PipeDirection.In, NamedPipeServerStream.MaxAllowedServerInstances))
            {
                var psi = new ProcessStartInfo("GetSitesFromIIS.exe")
                {
                    Arguments = string.Join(" ", toArchive),
                    Verb = "runas",
                    WindowStyle = ProcessWindowStyle.Normal,
                    UseShellExecute = true,
                    RedirectStandardOutput = false,
                    RedirectStandardError = false
                };
                var p = Process.Start(psi);

                var response = new MemoryStream(4*1024);

                pipe.WaitForConnection();
                while (!p.HasExited)
                {
                    if (pipe.IsConnected)
                    {
                        int b = pipe.ReadByte();
                        if (b > 0) // -1 means end of stream. 0 seems to be a null value, but it's not specified in documentation.
                        {
                            //Console.WriteLine(b);
                            response.WriteByte((byte) b);
                            Thread.Sleep(1); // Let's not max out the CPU!
                        }
                    }
                }
                response.Position = 0;
                var reader = new StreamReader(response);
                var s = reader.ReadToEnd();
                var kv = JsonConvert.DeserializeObject<Dictionary<string, string>>(s);
                Trace.WriteLine("Beginning backup process.");

                // Printout missing sites.
                var sitesNotInIis = toArchive.Where(x => !kv.Keys.Contains(x));
                Parallel.ForEach(sitesNotInIis, PrintMissingSiteMessage);

                // Do actual backup.
                Parallel.ForEach(kv, x=> ArchiveProcess(x.Key, x.Value, ArchivePath));
            }

            Trace.WriteLine("Archive process complete");
            Trace.WriteLine(Seperator);
            Console.WriteLine("Complete, press any key to exit.");
            Console.ReadKey();

        }

        private static void PrintMissingSiteMessage(string site)
        {
            Trace.WriteLine("The site '{0}' wasn't found in IIS - manual investigation will be required.");
        }

        private static void ArchiveProcess(string siteName, string physicalPath, string archivePath)
        {
            var timer = Stopwatch.StartNew();
            try
            {

                string folderName = string.Join(@"\", archivePath, siteName);

                if (!Directory.Exists(folderName))
                {
                    Directory.CreateDirectory(folderName);
                }

                ZipFile.CreateFromDirectory(physicalPath,
                    folderName + @"\" + siteName + ".zip",
                    CompressionLevel.Optimal,
                    true,
                    Encoding.UTF8);
                timer.Stop();
                Trace.WriteLine(string.Format("Finished archiving {0} after {1}", siteName, timer.Elapsed.ToString()));
            }
            catch (Exception ex)
            {
                Trace.WriteLine(string.Format("Failed archiving {0} with exception", siteName));
                Trace.WriteLine(Seperator);
                Trace.WriteLine(ex.ToString());
                Trace.WriteLine(Seperator);
            }
        }
    }
}
