﻿using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Web.Administration;
using Newtonsoft.Json;
using Shared.Extensions;

namespace GetSitesFromIIS
{
    class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Init");
            Console.WriteLine("Opening Pipe with handle: " + Shared.Constants.PipeName);
            using (var pipe = new NamedPipeClientStream(".", Shared.Constants.PipeName, PipeDirection.Out))
            {
                Console.WriteLine("Pipe initialized");
                pipe.Connect();
                Console.WriteLine("Pipe Connected");
                var w = GetSitePhysicalPaths(args.ToArray());
                Console.WriteLine("Site pathes found");
                var x = JsonConvert.SerializeObject(w);
                var y = x.GetBytes();

                pipe.Write(y, 0, y.Length);
                Console.WriteLine("Message Sent... Exiting");
            }
        }

        static Dictionary<string, string> GetSitePhysicalPaths(params string[] sites)
        {
            var iis = new ServerManager();
            return iis.Sites.Where(x => sites.Contains(x.Name)).ToDictionary(x => x.Name, x => x.Applications.First().VirtualDirectories.First().PhysicalPath);
        }
    }
}
